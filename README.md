# README #

1/3スケールのカシオ計算機 PV-2000風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- カシオ計算機

## 発売時期
- 1983年10月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PV-2000)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pv-2000/raw/a5d4382786321aae77a5113d9328bd2b2b47438c/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pv-2000/raw/a5d4382786321aae77a5113d9328bd2b2b47438c/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pv-2000/raw/a5d4382786321aae77a5113d9328bd2b2b47438c/ExampleImage.png)
